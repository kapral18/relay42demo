import template from './app.html';

const AppController = function (_, DateQueryService, EVENT, FakeAPIService, FakeSocketService, EventEmitterService) {

    this.$onInit = () => {

        this.unsubscribeList = [];

        this.setupListeners();

        FakeAPIService.fetchData()
            .then(this.transformResponse)
            .then(transformedResponse => {
                this.chartData = transformedResponse.chartData;
                this.chartLabels = transformedResponse.chartLabels;
            })
            .then(this.startFakeStream);
    };

    this.setupListeners = () => {
        this.unsubscribeList.push(
            EventEmitterService.subscribe(EVENT.SOCKET_CHART_UPDATE, data => {
                this.transformResponse([data]);
            })
        );
    };

    this.transformResponse = (rawResponseList) => {
        return _.reduce(rawResponseList, (acc, rawResponse) => {

            if (acc.chartLabels.length > 15) {

                acc.chartLabels.shift();
                acc.chartData[0].shift();
                acc.chartData[1].shift();
                acc.chartData[2].shift();
            }

            acc.chartLabels.push(DateQueryService.getShortDateFromMs(rawResponse.key.timestamp));
            acc.chartData[0].push(rawResponse.totalCallsAdded);
            acc.chartData[1].push(rawResponse.totalCallsRemoved);
            acc.chartData[2].push(rawResponse.segmentSize);

            return acc;

        }, {chartLabels: this.chartLabels || [], chartData: this.chartData || [[],[],[]]})
    };

    this.startFakeStream = () => {
        FakeSocketService.stream();
    };

    this.$onDestroy = () => {
        if (this.unsubscribeList.length) {
            _.forEach(this.unsubscribeList, unsubscribe => unsubscribe());
        }
    };
};

AppController.$inject = ['_', 'DateQueryService', 'EVENT', 'FakeAPIService', 'FakeSocketService', 'EventEmitterService'];

const AppComponent = {
    template,
    bindings: {},
    controller: AppController
};

export default AppComponent;