import 'bootstrap/dist/css/bootstrap.css';
import './main.scss';

import _ from 'lodash'
import angular from 'angular';
import 'chart.js';
import ngChart from 'angular-chart.js';
import AppComponent from './app.component';
import CommonModules from './common/common.module';
import ComponentsModule from './components/components.module';


const AppModule = angular.module('app', [CommonModules, ComponentsModule, ngChart])
    .component('app', AppComponent)
    .value('_', _)
    .name;

export default AppModule;