import angular from 'angular';

describe('App Module', () => {
    beforeEach(function() {
        angular.mock.module('app');
    });

    describe('Component: App', function () {

        let element, scope;

        beforeEach(angular.mock.inject(function ($rootScope, $compile) {
            scope = $rootScope.$new();
            element = $compile(angular.element('<app></app>'))(scope);
            scope.$apply();
        }));

        it('should exist', function () {
            expect(element.length).toBe(1);
        });


        describe('Controller', function () {
            let controller, FakeAPIService, deferred;
            beforeEach(angular.mock.inject(function($q, $componentController){
                controller = $componentController('app', { FakeAPIService: FakeAPIService }, {});
            }));

            it('should expose transformResponse method', function() {

                expect(controller.transformResponse).toBeDefined();
                expect(angular.isFunction(controller.transformResponse)).toBe(true);
            });

            it('should expose setupListeners method', function() {

                expect(controller.setupListeners).toBeDefined();
                expect(angular.isFunction(controller.setupListeners)).toBe(true);
            });

            it('should expose startFakeStream method', function() {

                expect(controller.startFakeStream).toBeDefined();
                expect(angular.isFunction(controller.startFakeStream)).toBe(true);
            });
        })
    });
});