import template from './chart.html';

const ChartController = function () {

    this.$onInit = () => {
        this.type = 'StackedBar';
        this.options = {
            scales: {
                xAxes: [
                    {
                        stacked: true,
                    }
                ],
                yAxes: [
                    {
                        stacked: true,
                        ticks: {
                            min: -100,
                            max: 100,
                            stepSize: 10
                        }
                    }
                ]
            },
            animation: false
        };

        this.datasetOverride = [
            {
                label: "Added Profiles",
                borderWidth: 1,
                type: 'bar'
            },
            {
                label: "Removed Profiles",
                borderWidth: 1,
                type: 'bar'
            },
            {
                label: "Selection size",
                borderWidth: 2,
                hoverBackgroundColor: "rgba(255,99,132,0.4)",
                hoverBorderColor: "rgba(255,99,132,1)",
                type: 'line'
            }
        ];
    };
};

const ChartComponent = {
    template,
    bindings: {
        data: '<',
        labels: '<'
    },
    controller: ChartController
};

export default ChartComponent;