import angular from 'angular';
import ChartModule from './chart/chart.module';

const ComponentsModule = angular
    .module('componentsModule', [ChartModule])
    .name;

export default ComponentsModule;