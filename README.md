## Realtime chartbar prototype ##

###### Prerequisites

`Node.js 8.0.0`

`npm v5`

Download [here](https://nodejs.org/en/)

###### Build View instructions (Mac OS)

1. `cd dist`
2. `open index.html`

###### Dev View instructions

1. `npm install`
2. `npm start`
3. Go to localhost:8080

The dev folder is `src`

###### Implementation remarks in brief

For this test project I used es6, angular 1.6.4, lodash 4 and created a component app structure.

For the barchart I used angular-graph.js.

I also made a small optimization in lifo(last in first out) manner to keep the graph from
ever expanding and crashing the browsers.

My understanding of the task was that I need to show realtime chart data management
something like, which I think I achieved.

There is lots to be refactored and improved obviously both in code structure and build
tools, also tests very importantly.

But my 2h budget doesn't allow me to fully engage with the project.