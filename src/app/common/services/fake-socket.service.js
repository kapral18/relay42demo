export default class FakeSocketService {

    constructor($timeout, _, EVENT, DateQueryService, EventEmitterService) {
        this._ = _;
        this.EVENT = EVENT;
        this.fakeCounterIndex = 5;
        this.$timeout = $timeout;
        this.DateQueryService = DateQueryService;
        this.EventEmitterService = EventEmitterService;
    }

    static get $inject() {
        return ['$timeout', '_', 'EVENT', 'DateQueryService', 'EventEmitterService'];
    }

    stream(oldTimeStamp) {

        this.$timeout(() => {

            let newTimeStamp;

            if (oldTimeStamp) {
                newTimeStamp = this.DateQueryService.getMsAfterAddingDays(1, oldTimeStamp);
            } else {
                newTimeStamp = this.DateQueryService.now();
            }

            this.EventEmitterService.publish(this.EVENT.SOCKET_CHART_UPDATE, {
                "key": {
                    "segmentNumber": this.fakeCounterIndex++,
                    "dayTimestamp": newTimeStamp,
                    "timestamp": newTimeStamp
                },
                "totalCallsAdded": this._.random(100, false),
                "totalCallsRemoved": this._.random(-100, -1, false),
                "segmentSize": this._.random(-100, 100, false)
            });

            this.stream(newTimeStamp);

        }, 1500);
    }
};