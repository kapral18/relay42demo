export default class DateQueryService {

    static get $inject() {
        return ['$window'];
    }

    constructor($window) {
        this.$window = $window;
    }

    getDaysAgoInMs(numberOfDays) {
        return this.$window.Date.now() - 60 * 60 * 24 * numberOfDays * 1e3;
    }

    getMsAfterAddingDays(numberOfDays, sinceWhenMs) {
        return sinceWhenMs + 60 * 60 * 24 * numberOfDays * 1e3
    }

    getShortDateFromMs(ms) {
        return new this.$window.Date(ms).toLocaleDateString('en-US', {
            month: 'short', day: 'numeric', year: 'numeric'
        });
    }

    now() {
        return this.$window.Date.now();
    }
};