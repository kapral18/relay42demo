// This file is an entry point for angular tests
// Avoids some weird issues when using webpack + angular.

import 'angular';
import 'angular-mocks';

const contextSrc = require.context('./app', true, /\.js$/);
contextSrc.keys().forEach(contextSrc);

