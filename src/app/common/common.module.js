import angular from 'angular';
import FakeAPIService from './services/fake-api.service';
import DateQueryService from './services/date-query.service';
import FakeSocketService from './services/fake-socket.service';
import EventEmitterService from './services/event-emitter.service';
import EVENT from './constants/event.constant';

const CommonModule = angular.module('commonModule', [])
    .service('FakeAPIService', FakeAPIService)
    .service('DateQueryService', DateQueryService)
    .service('FakeSocketService', FakeSocketService)
    .service('EventEmitterService', EventEmitterService)
    .constant('EVENT', EVENT)
    .name;

export default CommonModule;