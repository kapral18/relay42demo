export default class EventEmitterService {

    static get $inject () {
        return ['$timeout', '$rootScope', '_'];
    }

    constructor($timeout, $rootScope, _) {
        this.subscribers = {};
        this.$timeout = $timeout;
        this.$rootScope = $rootScope;
        this._ = _;
    }

    subscribe(topic, fn) {
        if (!this._.isString(topic) || !this._.isFunction(fn)) {
            return;
        }

        const fnList = [].concat(fn);

        if (this.subscribers.hasOwnProperty(topic)) {
            this.subscribers[topic] = this._.union(this.subscribers[topic], fnList);
        } else {
            this.subscribers[topic] = fnList;
        }

        return () => {
            this.subscribers[topic] = this._.difference(this.subscribers[topic], fnList);
        };
    }

    publish(topic, data) {
        this._.forEach(this.subscribers[topic], function (listener) {
            listener(data);
        });

        this.$rootScope.$evalAsync();
    }


};