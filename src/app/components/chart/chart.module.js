import angular from 'angular';
import ChartComponent from './chart.component';
import './chart.scss';

const ChartModule = angular.module('chartModule', [])
    .component('chart', ChartComponent)
    .name;

export default ChartModule;