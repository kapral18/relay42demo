export default class FakeAPIService {

    constructor($q, _, DateQueryService) {
        this._ = _;
        this.$q = $q;
        this.DateQueryService = DateQueryService;
    }

    static get $inject() {
        return ['$q', '_', 'DateQueryService'];
    }

    fetchData() {
        return this.$q((resolve) => {

            const data = this._.reduce([...new Array(5)], (acc, next, index) => {

                let newTimeStamp;
                if (this._.has(this._.last(acc), 'key')) {
                    let oldTimeStamp = this._.get(this._.last(acc), 'key.dayTimestamp');
                    newTimeStamp = this.DateQueryService.getMsAfterAddingDays(1, oldTimeStamp);
                } else {
                    newTimeStamp = this.DateQueryService.getDaysAgoInMs(5);
                }

                acc.push({
                    "key": {
                        "segmentNumber": index,
                        "dayTimestamp": newTimeStamp,
                        "timestamp": newTimeStamp
                    },
                    "totalCallsAdded": this._.random(100, false),
                    "totalCallsRemoved": this._.random(-100, -1, false),
                    "segmentSize": this._.random(-100, 100, false)
                });

                return acc;
            }, []);

            resolve(data);
        });
    }
};