const EVENT = {
    SOCKET_CHART_UPDATE: 'socket:chart:update'
};

export default EVENT;